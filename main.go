package main

import (
	"os"

	"gitlab.com/sensorbucket/go-ttn-worker/parser"
	"gitlab.com/sensorbucket/go-worker"
)

const (
	cfgAmqpURI       = "AMQP_URI"
	cfgAmqpQueue     = "AMQP_QUEUE"
	cfgAmqpExchange  = "AMQP_EXCHANGE"
	cfgManagementURI = "MANAGEMENT_URI"
)

func main() {
	etl := parser.New()

	worker.Start(&worker.Config{
		AMQPURI:  os.Getenv(cfgAmqpURI),
		Queue:    os.Getenv(cfgAmqpQueue),
		Exchange: os.Getenv(cfgAmqpExchange),
		MGMTURI:  os.Getenv(cfgManagementURI),
		ETL:      etl,
	})
}
