FROM golang:alpine AS builder

WORKDIR /opt

RUN apk add git --no-cache

# Install dependencies first for caching purposes
COPY go.mod go.mod
COPY go.sum go.sum
RUN go mod download

# Copy source code and build
COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o ./bin/ttn-worker

#
# Scratch docker image containing nothing but the application
#
FROM alpine AS production
# Start at /opt
WORKDIR /opt
# Copy our static executable.
COPY --from=builder /opt/bin/ttn-worker /opt/ttn-worker
# Run the hello binary.
CMD ["/opt/ttn-worker"]
