package parser

import (
	"errors"
	"fmt"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/sensorbucket/go-worker"
	"gitlab.com/sensorbucket/go-worker/pipeline"
)

// Parser ...
type Parser struct {
}

// New ...
func New() *Parser {
	return &Parser{}
}

// Parse parses a message
func (p *Parser) Parse(ctx *worker.ParseContext) error {
	var data HTTPPayload

	// Unmarshal into TTN Payload
	decoder, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		Metadata: nil,
		Result:   &data,
		TagName:  "json",
	})
	err = decoder.Decode(ctx.Message.Payload)
	if err != nil {
		return errors.New("Could not unmarshal into TTN Payload")
	}

	// Link this device against the internal ID
	dev, err := ctx.FindDevice(&worker.DeviceFilter{
		Source: map[string]interface{}{
			"deveui": data.Body.DeviceEUI,
		},
		Extra: map[string]string{
			"organisation": fmt.Sprint(ctx.Message.Owner),
		},
	})
	if err != nil {
		return fmt.Errorf("Could not fetch device for uplink message: %w", err)
	}

	ctx.Message.Device = dev
	// Parse payload to device data
	payload := &pipeline.DevicePayload{
		DeviceData: data.Body.PayloadRaw,
	}

	// Publish message
	err = ctx.Publish(pipeline.DeviceMessage, payload)
	if err != nil {
		return err
	}

	return ctx.Finish()
}
