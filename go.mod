module gitlab.com/sensorbucket/go-ttn-worker

go 1.15

// replace gitlab.com/sensorbucket/go-worker => /home/timvosch/go/src/gitlab.com/sensorbucket/go-worker

require (
	github.com/mitchellh/mapstructure v1.4.1
	gitlab.com/sensorbucket/go-worker v0.0.2
)
