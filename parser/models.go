package parser

// HTTPPayload is the payload that is received
type HTTPPayload struct {
	Headers map[string]string `json:"headers"`
	Body    struct {
		DeviceID   string `json:"dev_id"`
		DeviceEUI  string `json:"hardware_serial"`
		PayloadRaw string `json:"payload_raw"`
		Port       int    `json:"port"`
	} `json:"body"`
}
